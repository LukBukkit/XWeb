package de.lukweb.xweb.commands;

import de.lukweb.xweb.webserver.WebServer;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class XWebCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        if (!cs.hasPermission("xweb.admin")) {
            cs.sendMessage("You don't have the permission!");
            return true;
        }
        if (args.length < 1) {
            cs.sendMessage("You're using XWeb by LukBukkit.");
            cs.sendMessage("More commands: /xweb <start|stop|status>");
            return true;
        } else {
            switch (args[0].toLowerCase()) {
                case "start": {
                    if (WebServer.isRunning()) {
                        cs.sendMessage("The webserver is already running!");
                        break;
                    }
                    cs.sendMessage("The webserver will be started soon...");
                    WebServer.start();
                    break;
                }
                case "stop": {
                    if (!WebServer.isRunning()) {
                        cs.sendMessage("The webserver isnt't running!");
                        break;
                    }
                    WebServer.stop();
                    cs.sendMessage("Stopping the webserver...");
                    break;
                }
                case "status": {
                    String message = "The Webserver is ";
                    if (WebServer.isRunning()) {
                        message += ChatColor.GREEN + "running!";
                    } else {
                        message += ChatColor.RED + "stopped!";
                    }
                    cs.sendMessage(message);
                    break;
                }
                default: {
                    return false;
                }
            }
        }
        return true;
    }

}
