package de.lukweb.xweb;

import de.lukweb.xweb.commands.XWebCommand;
import de.lukweb.xweb.webserver.WebServer;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class XWeb extends JavaPlugin {

    private static XWeb instance;
    private static Logger logger;

    @Override
    public void onDisable() {
        WebServer.stop();
    }

    @Override
    public void onEnable() {

        instance = this;
        logger = this.getLogger();

        getCommand("xweb").setExecutor(new XWebCommand());

        saveDefaultConfig();
        copyIndex();

        if (getConfig().getBoolean("autostart") || !getConfig().isSet("autostart")) {
            startWebServer();
        }

    }

    private void startWebServer() {

        logger.log(Level.INFO, "Starting Webserver...");

        new BukkitRunnable() {
            @Override
            public void run() {
                WebServer.start();
            }
        }.runTaskAsynchronously(this);

    }

    private void stopServer() {

    }

    private void copyIndex() {
        if (!new File(getDataFolder(), "www").exists()) {
            try {
                InputStream is = getResource("index.html");
                byte[] buffer = new byte[is.available()];
                is.read(buffer);
                is.close();

                OutputStream os = new FileOutputStream(new File(getWWWFolder(), "index.html"));
                os.write(buffer);
                os.close();

            } catch (Exception ex) {
                getLogger().log(Level.WARNING, "Error coping the default index.html.");
                ex.printStackTrace();
            }
        }
    }

    public static File getWWWFolder() {
        File f = new File(XWeb.getInstance().getDataFolder(), "www");
        f.mkdirs();
        return f;
    }

    public static Logger getPlLogger() {
        return logger;
    }

    public static XWeb getInstance() {
        return instance;
    }
}