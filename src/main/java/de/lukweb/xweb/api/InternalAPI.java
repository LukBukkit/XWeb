package de.lukweb.xweb.api;

import com.sun.net.httpserver.HttpExchange;
import de.lukweb.xweb.XWebHandler;
import de.lukweb.xweb.api.objects.PluginWebHandler;
import de.lukweb.xweb.api.objects.WebRequest;

import java.io.IOException;
import java.util.HashMap;

public class InternalAPI {

    private static HashMap<String, PluginWebHandler> webHandler = new HashMap<>();

    public static HashMap<String, PluginWebHandler> getHM() {
        return webHandler;
    }

    public static void handle(HttpExchange ex) {
        try {
            for (String string : webHandler.keySet()) {
                if (trimPath(ex.getRequestURI().toString()).startsWith(string)) {
                    webHandler.get(string).getHandler().handle(new WebRequest(ex)).send(ex);
                    return;
                }
            }
            new XWebHandler().handle(new WebRequest(ex)).send(ex);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String trimPath(String path) {
        if (path.startsWith("\\") || path.startsWith("/")) {
            return path.substring(1).toLowerCase();
        }
        return path.toLowerCase();
    }

}
