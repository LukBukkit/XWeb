package de.lukweb.xweb.api;

import de.lukweb.xweb.api.objects.PluginWebHandler;
import org.bukkit.plugin.Plugin;

import java.util.Iterator;

public class XWebAPI {

    /**
     * Register your handler to a specific path
     * @param path - The path e.g. /<YOURPATH>/
     * @param handler - Your handler
     * @param plugin - Your plugin object
     */
    public static void register(String path, WebHandler handler, Plugin plugin) {
        InternalAPI.getHM().put(path.toLowerCase(), new PluginWebHandler(path, handler, plugin.getName()));
    }

    /**
     * Unregister all handlers for a specific path
     * @param path - The path
     */
    public static void unregister(String path) {
        Iterator<String> it = InternalAPI.getHM().keySet().iterator();
        while (it.hasNext()) {
            String string = it.next();
            if (string.equalsIgnoreCase(path)) {
                it.remove();
            }
        }
    }

    /**
     * Unregister all handlers owned by a specific plugin
     * @param plugin - The plugin object
     */
    public static void unregister(Plugin plugin) {
        Iterator<PluginWebHandler> it = InternalAPI.getHM().values().iterator();
        while (it.hasNext()) {
            PluginWebHandler plhandler = it.next();
            if (plhandler.getPlugin().equalsIgnoreCase(plugin.getName())) {
                it.remove();
            }
        }
    }

}
