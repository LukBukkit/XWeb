package de.lukweb.xweb.api;

import de.lukweb.xweb.api.objects.WebAnswer;
import de.lukweb.xweb.api.objects.WebRequest;

public interface WebHandler {

    public WebAnswer handle(WebRequest request);

}
