package de.lukweb.xweb.api;

import de.lukweb.xweb.XWeb;

import java.io.*;

public class FileUtils {

    public static String getExtension(File f) {
        String extension = "";

        int i = f.getName().lastIndexOf('.');
        int p = Math.max(f.getName().lastIndexOf('/'), f.getName().lastIndexOf('\\'));

        if (i > p) {
            extension = f.getName().substring(i + 1);
        }
        return extension;
    }

    public static String getFileString(File f) {
        StringBuilder contentBuilder = new StringBuilder();
        try {
            BufferedReader in = new BufferedReader(new FileReader(f));
            String str;
            while ((str = in.readLine()) != null) {
                contentBuilder.append(str);
            }
            in.close();
        } catch (IOException e) {
            XWeb.getInstance().getLogger().warning("WebServer: Cannot read file " + f.getAbsolutePath());
        }
        return contentBuilder.toString();

    }

    public static byte[] getFileBytes(File f) {
        byte[] fileBytes = null;
        try {
            BufferedInputStream in = new BufferedInputStream(new FileInputStream(f));
            fileBytes = new byte[(int) f.length()];
            in.read(fileBytes);
            in.close();
        } catch (IOException e) {
            XWeb.getInstance().getLogger().warning("WebServer: Cannot read file " + f.getAbsolutePath());
        }
        return fileBytes;
    }

}
