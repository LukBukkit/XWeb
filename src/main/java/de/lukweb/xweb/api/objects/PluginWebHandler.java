package de.lukweb.xweb.api.objects;

import de.lukweb.xweb.api.WebHandler;

public class PluginWebHandler {

    private String path;
    private WebHandler handler;
    private String plugin;

    public PluginWebHandler(String path, WebHandler handler, String plugin) {
        this.path = path;
        this.handler = handler;
        this.plugin = plugin;
    }

    /**
     *
     * @return The requested path e.g: /hello/
     */
    public String getPath() {
        return path;
    }

    /**
     *
     * @return The handler registered for this path
     */
    public WebHandler getHandler() {
        return handler;
    }

    /**
     *
     * @return The plugin object owing the handler
     */
    public String getPlugin() {
        return plugin;
    }
}
