package de.lukweb.xweb.api.objects;

public enum WebReturnType {

    CONTINUE(100),
    PROCESSING(102),
    OK(200),
    MOVED_PERMANENTLY(301),
    BAD_REQUEST(400),
    UNAUTHORIZED(401),
    FORBIDDEN(403),
    NOT_FOUND(404),
    METHOD_NOT_ALLOWED(405),
    REQUEST_TIMEOUT(408),
    GONE(410),
    LENGTH_REQUIRED(411),
    LOCKED(423),
    TOO_MANY_REQUESTS(429),
    INTERNAL_SERVER_ERROR(500),
    NOT_IMPLEMENTED(501),
    BAD_GATEWAY(502),
    SERVICE_UNAVAILABLE(503);

    private int statuscode;

    WebReturnType(int statuscode) {
        this.statuscode = statuscode;
    }

    /**
     *
     * @return The status code
     */
    public int getStatusCode() {
        return statuscode;
    }

}
