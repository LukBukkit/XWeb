package de.lukweb.xweb.api.objects;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;

import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.URI;

public class WebRequest {

    private HttpExchange ex;

    public WebRequest(HttpExchange ex) {
        this.ex = ex;
    }

    /**
     *
     * @return The adress of the user requeseted a web page
     */
    public InetSocketAddress getRequester() {
        return ex.getRemoteAddress();
    }

    /**
     *
     * @return The path requested by the use
     */
    public URI getRequestPath() {
        return ex.getRequestURI();
    }

    /**
     *
     * @return The request method. In the most cases this is GET. Sometimes it's POST.
     */
    public String getRequestMethod() {
        return ex.getRequestMethod();
    }

    /**
     *
     * @return The body of the request
     */
    public InputStream getRequestBody() {
        return ex.getRequestBody();
    }

    /**
     *
     * @return The headers of the request
     */
    public Headers getRequestHeaders() {
        return ex.getRequestHeaders();
    }

    /**
     *
     * @return The HttpExchange object. Use this only, if you need more informations about the request
     */
    public HttpExchange getHttpExchange() {
        return ex;
    }

}
