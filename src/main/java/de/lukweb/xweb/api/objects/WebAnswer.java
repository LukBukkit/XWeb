package de.lukweb.xweb.api.objects;

import com.sun.net.httpserver.HttpExchange;
import de.lukweb.xweb.api.FileUtils;
import de.lukweb.xweb.webserver.MimeType;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

public class WebAnswer {

    private byte[] data;
    private WebReturnType type;
    private MimeType mimeType;

    /**
     * This is an answer for the user requsting the website
     *
     * @param type - The status code for the return
     * @param data - The file contaning the data. The file will be read and sended to the user.
     */
    public WebAnswer(WebReturnType type, File data) {
        this.data = FileUtils.getFileBytes(data);
        this.type = type;
        this.mimeType = MimeType.valueOf(FileUtils.getExtension(data));
    }

    /**
     * This is an answer for the user requsting the website
     *
     * @param type - The status code for the return
     * @param data - The answer which will be sended to the user. It will be sended as an HTML-Document. Every entry in the array will be a line.
     */
    public WebAnswer(WebReturnType type, String[] data) {
        this(type, data, MimeType.html);
    }

    /**
     * This is an answer for the user requsting the website
     *
     * @param type     - The status code for the return
     * @param data     - The answer which will be sended to the user. Every entry in the array will be a line.
     * @param mimeType - The MimeType for the answer
     */
    public WebAnswer(WebReturnType type, String[] data, MimeType mimeType) {
        StringBuilder builder = new StringBuilder();
        for (String s : data) {
            builder.append(s + "\n");
        }
        this.data = builder.toString().getBytes(StandardCharsets.UTF_8);
        this.type = type;
        this.mimeType = mimeType;
    }

    /**
     * This is an answer for the user requsting the website
     *
     * @param type - The status code for the return
     * @param data - The answer which will be sended to the user.
     */
    public WebAnswer(WebReturnType type, String data) {
        this(type, data, MimeType.html);
    }

    /**
     * This is an answer for the user requsting the website
     *
     * @param type     - The status code for the return
     * @param data     - The answer which will be sended to the user.
     * @param mimeType - The MimeType for the answer
     */
    public WebAnswer(WebReturnType type, String data, MimeType mimeType) {
        this.data = data.getBytes(StandardCharsets.UTF_8);
        this.type = type;
    }

    /**
     * This is an answer for the user requsting the website
     *
     * @param type     - The status for the return
     * @param data     - The data sended to the user
     * @param mimeType - The MimeType for the data
     */
    public WebAnswer(WebReturnType type, byte[] data, MimeType mimeType) {
        this.data = data;
        this.type = type;
        this.mimeType = mimeType;
    }

    /**
     * This is an answer for the user requsting the website. Use this constructor only when the status code isn't 200. So that I can display an custom error page.
     *
     * @param type - The status code for the answer
     */
    public WebAnswer(WebReturnType type) {
        this.type = type;
    }

    public void send(HttpExchange ex) throws IOException {
        if (data != null) {
            ex.getResponseHeaders().add("Content-type", mimeType.contentType);
            ex.sendResponseHeaders(type.getStatusCode(), data.length);
            OutputStream stream = ex.getResponseBody();
            stream.write(data);
            stream.close();
        } else {
            ex.sendResponseHeaders(type.getStatusCode(), 0);
            OutputStream stream = ex.getResponseBody();
            stream.write((byte) 0);
            stream.close();
        }
    }

}
