package de.lukweb.xweb.webserver;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;
import de.lukweb.xweb.XWeb;
import de.lukweb.xweb.net.NetUtils;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WebServer {

    private static boolean running;
    private static HttpServer server;

    public static void start() {
        XWeb instance = XWeb.getInstance();
        Logger logger = XWeb.getPlLogger();
        if (!instance.getConfig().isSet("port")) {
            logger.log(Level.WARNING, "Cannot start Webserver: The port isn't defined. Just delete the config file of this plugin.");
            return;
        }

        int port = instance.getConfig().getInt("port");

        if (!NetUtils.isPortOpen(port)) {
            logger.log(Level.WARNING, "Cannot start Webserver: The port " + port + " is already in usage! Please check all running application on your server or choose another port!");
            return;
        }

        try {
            server = HttpServer.create(new InetSocketAddress(port), 0);
            HttpContext httpContext = server.createContext("/", new WebHandler());
            httpContext.getFilters().add(new PostFilter());
            server.start();
            running = true;
            logger.log(Level.INFO, "Webserver is running!");
        } catch (IOException e) {
            logger.log(Level.WARNING, "Cannot start Webserver: Unknown Error. Please report this bug with the following error @ https://lukweb.de/short/xwebserver");
            e.printStackTrace();
        }
    }

    public static void stop() {
        server.stop(0);
        running = false;
    }

    public static boolean isRunning() {
        return running;
    }

}
