package de.lukweb.xweb.webserver;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import de.lukweb.xweb.api.InternalAPI;

import java.io.IOException;

public class WebHandler implements HttpHandler {

    @Override
    public void handle(HttpExchange ex) throws IOException {

        InternalAPI.handle(ex);

    }

}
