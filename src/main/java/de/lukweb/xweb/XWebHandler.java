package de.lukweb.xweb;

import de.lukweb.xweb.api.WebHandler;
import de.lukweb.xweb.api.objects.WebAnswer;
import de.lukweb.xweb.api.objects.WebRequest;
import de.lukweb.xweb.api.objects.WebReturnType;

import java.io.File;

public class XWebHandler implements WebHandler {

    @Override
    public WebAnswer handle(WebRequest request) {

        File wwwdir = XWeb.getWWWFolder();

        File reqfile = new File(wwwdir, request.getRequestPath().toString().toLowerCase());

        if (!reqfile.exists()) {
            return new WebAnswer(WebReturnType.NOT_FOUND);
        }

        if (reqfile.isDirectory()) {
            File indexdir = new File(reqfile, "index.html");
            if (!indexdir.exists()) {
                return new WebAnswer(WebReturnType.NOT_FOUND); // Maybe change later
            }
            return new WebAnswer(WebReturnType.OK, indexdir);
        }

        if (reqfile.isFile()) {
            return new WebAnswer(WebReturnType.OK, reqfile);
        }

        return new WebAnswer(WebReturnType.INTERNAL_SERVER_ERROR);
    }

}
