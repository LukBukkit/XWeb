# XWeb
A simple webserver for Spigot.

## You are a server owner?
You find more on [Spigot](https://www.spigotmc.org/resources/18773/)!

## Found a bug?
You can report it [here](https://gitlab.com/LukBukkit/XWeb/issues). You have to be logged in to create a new issue.

## Compile it yourself
Clone this git repository and execute in the main directory the command `mvn install`. To execute this command you'll JDK 7 or higher and maven. You can find the compiled jar in the target folder.
